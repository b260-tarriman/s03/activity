<?php 

class Person{
    public $firstName;
    public $middleName;
    public $lastName;

    public function __construct($firstName,$middleName,$lastName){
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    public function printName(){
        return "Your full name is $this->lastName, $this->firstName $this->middleName";
    }
}

class Developer extends Person{
    public function printName(){
        return "Your full name is $this->lastName, $this->firstName $this->middleName and you are a developer";
    }
}

class Engineer extends Person{
    public function printName(){
        return "You are an engineer named $this->lastName, $this->firstName $this->middleName";
    }
}

$person = new Person('Senku','Yu',"Ishigami");
$dev = new Developer('John','Finch',"Smith");
$engineer = new engineer('Harold','Myers',"Reese");