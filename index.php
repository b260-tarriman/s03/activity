<?php require_once './code.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S03: Classes and Objects Activity</title>
</head>
<body>
    <h3>Person</h3>
    <p> <?= $person->printName() ?></p>
    <h3>Developer</h3>
    <p> <?= $dev->printName() ?></p>
    <h3>Engineer</h3>
    <p> <?= $engineer->printName() ?></p>
</body>
</html>